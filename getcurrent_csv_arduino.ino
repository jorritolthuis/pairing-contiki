#include <Wire.h>
#include <Adafruit_INA219.h>

Adafruit_INA219 ina219;
int i=0;
unsigned long start;
char enabled = 0;
char finished = 0;
enum trigger_phase {
  pre_boot = 0, // Initial state
  power_up, // State where the board has restarted, and Contiki is starting up
  sleep, // State right before the main execution, a 2 second sleep, we will record the last .5 seconds of this
  measuring // Performing the measurement
};
enum trigger_phase trigger_state = pre_boot;
unsigned long trigger_sleep_start = 0;

//#define SEPARATE_CURRENT
// The idea of registering current and voltage in separate events is that
// we can fit more into memory when the ratio between the two events is 
// 1:2 or more. Note that this does bring the complexity of estimating the
// ratio between the two events to pre-allocate the arrays.

#define BUFFER_SIZE 88000 // The Due has 96KB of SRAM

#define PREV_IDX(idx, len) (idx-1>=0  ? idx-1 : len-1)
#define NEXT_IDX(idx, len) (idx+1<len ? idx+1 : 0)

#ifndef SEPARATE_CURRENT
  typedef struct {
    int t; // delta t
    float v; // voltage
    float i; // current
  } datapoint_t;

  #define RING_LEN (BUFFER_SIZE/sizeof(datapoint_t))
  datapoint_t ring_buffer[RING_LEN];
  int ring_idx = 0;
  #define PREV_RING_IDX PREV_IDX(ring_idx, RING_LEN)
  #define NEXT_RING_IDX NEXT_IDX(ring_idx, RING_LEN)

  int start_idx = 0;
#else /* SEPARATE_CURRENT */
  #define CURRENT_RATIO 5 // Estimation of how many current measurements come in for every one voltage measurement
  
  typedef struct {
    int t;
    float i;
  } currentpoint_t;
  
  typedef struct {
    int t;
    float v;
  } voltagepoint_t;

  #define RING_I_LEN (CURRENT_RATIO*BUFFER_SIZE/(CURRENT_RATIO+1)/sizeof(currentpoint_t))
  #define RING_V_LEN (1*BUFFER_SIZE/(CURRENT_RATIO+1)/sizeof(voltagepoint_t))
  currentpoint_t current_buffer[RING_I_LEN];
  voltagepoint_t voltage_buffer[RING_V_LEN];
  int current_idx = 0;
  int voltage_idx = 0;
  #define PREV_I_IDX PREV_IDX(current_idx, RING_I_LEN)
  #define NEXT_I_IDX NEXT_IDX(current_idx, RING_I_LEN)
  #define PREV_V_IDX PREV_IDX(voltage_idx, RING_V_LEN)
  #define NEXT_V_IDX NEXT_IDX(voltage_idx, RING_V_LEN)

  int start_i_idx = 0;
  int start_v_idx = 0;
#endif /* SEPARATE_CURRENT */




// From https://learn.adafruit.com/memories-of-an-arduino/measuring-free-memory
#ifdef __arm__
// should use uinstd.h to define sbrk but Due causes a conflict
extern "C" char* sbrk(int incr);
#else  // __ARM__
extern char *__brkval;
#endif  // __arm__

int freeMemory() {
  char top;
#ifdef __arm__
  return &top - reinterpret_cast<char*>(sbrk(0));
#elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
  return &top - __brkval;
#else  // __arm__
  return __brkval ? &top - __brkval : &top - __malloc_heap_start;
#endif  // __arm__
}





void setup(void) 
{
  Serial.begin(115200);
  while (!Serial) {
      // will pause Zero, Leonardo, etc until serial console opens
      delay(1);
  }
  
  if (! ina219.begin()) {
    Serial.println("Failed to find INA219 chip");
    while (1) { delay(10); }
  }
  ina219.setCalibration_16V_200mA();

  Serial.println("Free memory: ");
  Serial.println(freeMemory());

#ifndef SEPARATE_CURRENT
  for (int i=0; i<RING_LEN; ++i) {
    ring_buffer[i].t = 0;
    ring_buffer[i].v = 0;
    ring_buffer[i].i = 0;
  }
#else /* SEPARATE_CURRENT */
#error Should start by setting everything to 0
#endif /* SEPARATE_CURRENT */
  
  start = micros();
}





void loop(void) 
{
  if (finished) return;
  
  /* Start by retrieving new measurements */
  float shuntvoltage = ina219.getShuntVoltage_mV();
  float busvoltage   = ina219.getBusVoltage_V();
  float current      = ina219.getCurrent_mA();
  float loadvoltage  = busvoltage + (shuntvoltage / 1000);


  /* See if we need to update the ring buffer */
#ifndef SEPARATE_CURRENT
  if (current != ring_buffer[PREV_RING_IDX].i ||
      loadvoltage != ring_buffer[PREV_RING_IDX].v) {
    // We need to update
    unsigned long time = micros();

    ring_buffer[ring_idx].t = time-start;
    ring_buffer[ring_idx].i = current;
    ring_buffer[ring_idx].v = loadvoltage;
    
    ring_idx = NEXT_RING_IDX;
    start = time;
  } else {
    // No need to update, skip automatic trigger
    return;
  }
#else /* SEPARATE_CURRENT */
  char update = 0;
  if (current != current_buffer[PREV_I_IDX].i) {
    unsigned long time = micros();

    current_buffer[current_idx].t = time-start;
    current_buffer[current_idx].i = current;
    
    current_idx = NEXT_I_IDX;
    start = time;
    update++;
  }

  if (loadvoltage != voltage_buffer[PREV_V_IDX].v) {
    unsigned long time = micros();

    voltage_buffer[voltage_idx].t = time-start;
    voltage_buffer[voltage_idx].v = loadvoltage;
    
    voltage_idx = NEXT_V_IDX;
    start = time;
    update++;
  }

  if (update == 0) {
    // We did not do any update
    return;
  }
#endif /* SEPARATE_CURRENT */

  /* Automatic trigger */
  if (!enabled) enabled = 1;
  // The code below should offer a fancy automatic trigger to start recording at the proper time.
  // This doesn't quite work, so I have used the simple trigger above, and reset the RE-Mote right
  // after restarting the Arduino.
  // An alternative simple trigger could also involve a sleep to skip the startup phase, since
  // the relevant operation in my case is so short, a sleep is definitely not necessary.
  if (!enabled) {
    // Simplest trigger
    switch(trigger_state) {
      case pre_boot:
#ifndef SEPARATE_CURRENT
        if (ring_buffer[ring_idx].v * ring_buffer[ring_idx].i - 
            ring_buffer[PREV_IDX(PREV_IDX(PREV_IDX(PREV_RING_IDX, RING_LEN), RING_LEN), RING_LEN)].v * ring_buffer[PREV_IDX(PREV_IDX(PREV_IDX(PREV_RING_IDX, RING_LEN), RING_LEN), RING_LEN)].i
            >= 35) {
#else /* SEPARATE_CURRENT */
#error No trigger defined
#endif /* SEPARATE_CURRENT */
          trigger_state = power_up;
          Serial.println("Detected device powering up.");
        }
        break;
      case power_up:
#ifndef SEPARATE_CURRENT
        if (ring_buffer[ring_idx].v * ring_buffer[ring_idx].i < 110) {
#else /* SEPARATE_CURRENT */
#error No trigger defined
#endif /* SEPARATE_CURRENT */
          trigger_state = sleep;
          Serial.println("Detected device going to sleep.");
        }
        break;
      case sleep:
        if (trigger_sleep_start == 0) trigger_sleep_start = micros();
        if (micros() - trigger_sleep_start > 1500000) { // Record the last 0.5 seconds of 2 seconds sleep
          enabled = 1;
          trigger_state = measuring;
          Serial.println("Starting measurement");
        }
        break;
      default:
      case measuring:
        break;
    }

#ifndef SEPARATE_CURRENT
    start_idx = ring_idx;
#else /* SEPARATE_CURRENT */
    // We need to create a common starting point where both are set
    current_buffer[PREV_I_IDX].t = 0;
    current_buffer[PREV_I_IDX].i = current;
    voltage_buffer[PREV_V_IDX].t = 0;
    voltage_buffer[PREV_V_IDX].v = loadvoltage;

    start_i_idx = current_idx;
    start_v_idx = voltage_idx;
#endif /* SEPARATE_CURRENT */
  }


  /* Print when the buffer is full */
  if (!enabled) return;
#ifndef SEPARATE_CURRENT
  if (ring_idx != PREV_IDX(start_idx, RING_LEN)) {
    return;
  }

#else /* SEPARATE_CURRENT */
  char current_full = current_idx == PREV_IDX(start_i_idx, RING_I_LEN);
  char voltage_full = voltage_idx == PREV_IDX(start_v_idx, RING_V_LEN);

  if (current_full) {Serial.println("Current full");Serial.print("Room left (start): ");Serial.print(start_v_idx);Serial.print(" (end):");Serial.print(current_idx);Serial.print(" (size):");Serial.println(RING_I_LEN);}
  if (voltage_full) {Serial.println("Voltage full");Serial.print("Room left (start): ");Serial.print(start_i_idx);Serial.print(" (end):");Serial.print(voltage_idx);Serial.print(" (size):");Serial.println(RING_V_LEN);}
    
  if (!current_full && !voltage_full) {
    return;
  }
#endif /* SEPARATE_CURRENT */
  // If the buffer is not yet full, we will have returned before this point
  enabled = 0;
  finished = 1;
  Serial.println("dt, current, voltage");

#ifndef SEPARATE_CURRENT
  int print_idx = start_idx;

  while (print_idx != PREV_IDX(start_idx, RING_LEN)) {
    Serial.print(ring_buffer[print_idx].t);
    Serial.print(", ");
    Serial.print(ring_buffer[print_idx].i, 4);
    Serial.print(", ");
    Serial.println(ring_buffer[print_idx].v, 4);
    print_idx = NEXT_IDX(print_idx, RING_LEN);
  }
#else /* SEPARATE_CURRENT */
  int print_i_idx = start_i_idx;
  int print_v_idx = start_v_idx;
  unsigned long tv = 0;
  unsigned long ti = 0;

  // Print first idx
  Serial.print("0, ");
  Serial.print(current_buffer[print_i_idx].i, 4);
  Serial.print(", ");
  Serial.println(voltage_buffer[print_v_idx].v, 4);
  float last_current = current_buffer[print_i_idx].i;
  float last_voltage = voltage_buffer[print_v_idx].v;
  print_i_idx = NEXT_IDX(print_i_idx, RING_I_LEN);
  print_v_idx = NEXT_IDX(print_v_idx, RING_V_LEN);
  
  while (print_i_idx != PREV_IDX(start_i_idx, RING_I_LEN) &&
         print_v_idx != PREV_IDX(start_v_idx, RING_V_LEN)) {
    int dt;
    float cur, volt;
    if (tv + voltage_buffer[print_v_idx].t < ti + current_buffer[print_i_idx].t) {
      // First print updated voltage
      dt = voltage_buffer[print_v_idx].t;
      cur = last_current;
      volt = voltage_buffer[print_v_idx].v;
      
      tv += dt;
      last_voltage = volt;
      print_v_idx = NEXT_IDX(print_v_idx, RING_V_LEN);
    } else {
      // First print updated current
      dt = current_buffer[print_i_idx].t;
      cur = current_buffer[print_i_idx].i;
      volt = last_voltage;
      
      ti += dt;
      last_current = cur;
      print_i_idx = NEXT_IDX(print_i_idx, RING_I_LEN);
    }
    
    Serial.print(dt);
    Serial.print(", ");
    Serial.print(cur, 4);
    Serial.print(", ");
    Serial.println(volt, 4);
  }

  // There may be some samples from either current/voltage left.
  // But assuming both regularly change (even due to noise), there
  // can't be much left.

#endif /* SEPARATE_CURRENT */
}










/****************************************************************************************
 * INSERT THE FOLLOWING CODE INTO AdaFruit_INA219.cpp and the corresponding header file *
 ****************************************************************************************

*************************
*** AdaFruit_INA219.h ***
*************************
*** Set as public member function of the Adafruit_INA219 class ***

void setCalibration_16V_200mA();

***************************
*** AdaFruit_INA219.cpp ***
***************************

void Adafruit_INA219::setCalibration_16V_200mA() {
  // Calibration which uses the highest precision for
  // current measurement (0.1mA), at the expense of
  // only supporting 16V at 200mA max (in fact it supports ~325mA).

  // VBUS_MAX = 16V
  // VSHUNT_MAX = 0.04          (Assumes Gain 1, 40mV)
  // RSHUNT = 0.1               (Resistor value in ohms)

  // 1. Determine max possible current
  // MaxPossible_I = VSHUNT_MAX / RSHUNT
  // MaxPossible_I = 0.4A

  // 2. Determine max expected current
  // MaxExpected_I = 0.2A
  // ^^^ This is where we gain more precision over the 16/400 calibration.
  //     We exchange some current range (i.e. overflow faster) for more precision.

  // 3. Calculate possible range of LSBs (Min = 15-bit, Max = 12-bit)
  // MinimumLSB = MaxExpected_I/32767
  // MinimumLSB = 0.0000061              (6.1uA per bit)
  // MaximumLSB = MaxExpected_I/4096
  // MaximumLSB = 0.0000488              (49uA per bit)

  // 4. Choose an LSB between the min and max values
  //    (Preferrably a roundish number close to MinLSB)
  // CurrentLSB = 0.00001 (10uA per bit) 
  // ^^^ 5x more precision over 16/400
  
  // 5. Compute the calibration register
  // Cal = trunc (0.04096 / (Current_LSB * RSHUNT))
  // Cal = 40960 (0xA000)

  ina219_calValue = 40960;

  // 6. Calculate the power LSB
  // PowerLSB = 20 * CurrentLSB
  // PowerLSB = 0.0002 (200uW per bit)

  // 7. Compute the maximum current and shunt voltage values before overflow
  //
  // Max_Current = Current_LSB * 32767
  // Max_Current = 327.67mA before overflow
  //
  // If Max_Current > Max_Possible_I then
  //    Max_Current_Before_Overflow = MaxPossible_I
  // Else
  //    Max_Current_Before_Overflow = Max_Current
  // End If
  //
  // Max_Current_Before_Overflow = MaxPossible_I
  // Max_Current_Before_Overflow = 0.327

  // Set multipliers to convert raw current/power values
  ina219_currentDivider_mA = 100;    // Current LSB = 10uA per bit (1000/10 = 100)
  ina219_powerMultiplier_mW = 0.2f;  // Power LSB = 200uW per bit

  // Set Calibration register to 'Cal' calculated above
  Adafruit_BusIO_Register calibration_reg =
      Adafruit_BusIO_Register(i2c_dev, INA219_REG_CALIBRATION, 2, MSBFIRST);
  calibration_reg.write(ina219_calValue, 2);
  // Set Config register to take into account the settings above
  uint16_t config = INA219_CONFIG_BVOLTAGERANGE_16V |
                    INA219_CONFIG_GAIN_1_40MV | INA219_CONFIG_BADCRES_12BIT |
                    INA219_CONFIG_SADCRES_12BIT_1S_532US |
                    INA219_CONFIG_MODE_SANDBVOLT_CONTINUOUS;

  Adafruit_BusIO_Register config_reg =
      Adafruit_BusIO_Register(i2c_dev, INA219_REG_CONFIG, 2, MSBFIRST);
  _success = config_reg.write(config, 2);
}

*/
