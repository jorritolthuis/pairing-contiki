# Pairing analysis

This project makes a comparison between using hardware acceleration versus a full software implementation for pairing based operations. The test device is a CC2538 on Contiki-NG 4.7.
Clone this repository in the `examples/` directory in Contiki-NG.

This project makes use of the GMP and PBC libraries from https://github.com/wellsaid. Some changes based on this were made to change build behaviour in the `contiki_ng` branch of https://gitlab.com/jorritolthuis/cpabe-bsw.

**IMPORTANT: Set `LPM_CONF_ENABLE` to `0` and `LPM_CONF_MAX_PM` to `1` in `arch/platform/zoul/contiki-conf.h`.**

## Flags

These FLAGS can be set as the environment for the `build.sh` script.

* Set `TARGET` to the target to build for.
* Set `BOARD` if required by the `TARGET`.
* Set `CONTIKI_RANDOM=1` to use the Contiki RNG instead of the implemention in PBC. Default enabled when not building native.
* Set `HW_[OPERATION]=1` to enable HW acceleration of the CC2538 for some OPERATION. Any of the following.
  * `MODINV` to enable hardware acceleration for modular inversions.
  * `ELTPOW` to enable hardware acceleration for modular exponentiation (the name may be slightly misleading).
  * `ECC` to enable hardware acceleration for ECC curve operations (especially effective for type F pairings), not used for setup, only for pairing.
  * `MUL` to enable hardware acceleration for modular multiplications (especially effective for type A pairings), works by default only for `TYPE_A`.
* Set `NO_STRIP=1` to skip the step where the executable is stripped from unused elements to reduce size. By default, ROM size is reduced using this method.
* Set `DETERMINISTIC=1` to use deterministic input. By default, input will be random.
* Set `TIMING=1` to determine the execution time of the pairing operation using rtimer (CC2538-specific).
* Set `POWER=1` to sleep before and after the operation, this should make the operation easier to identify in a power graph. **Note: additionally, set `LPM_CONF_ENABLE` to 1 in `arch/platform/zoul/contiki-conf.h`.**
* Set `KEEP_PKA_ON=1` to ensure that the PKA is not disabled between HW operations (only has an effect when a `HW_` flag is enabled).
* Set `SLEEP_DURING_HW=1` to make the board go to sleep during HW operations (only has an effect when a `HW_` flag is enabled).
* Set `TYPE_A_318=1` to use type A curve of equal security level to type F.
* Set `TYPE_A_512=1` to use minimum recommended security level (default).
* Set `TYPE_A_700=1` to use type A curve at (roughly) highest security level without crashing.
* Set `TYPE_F=1` to use type F curve.

## Native build

Execute the following commands to build the _native_ full software implementation using the libraries GMP and PBC. The native executable runs without a board.

To compile, run `TARGET=native ./build.sh`. The file `fib_table.c` will fail, but since this test is not using Fibonacci numbers, there is no real problem. Check out https://gitlab.com/jorritolthuis/cpabe-bsw to see all include paths required for GMP in Contiki-NG.

## CC2538 build

The build process has been set up to also compile for boards running the CC2538 CPU. Currently, openmote and zoul TARGETs have been handled.

To build, run for example `TARGET=zoul BOARD=remote-revb ./build.sh`. Note that the `CONTIKI_RANDOM` is always enabled, as the program cannot run with the default PBC RNG.

**WARNING:** Upload using the command shown at the end of build.sh, otherwise some compilation flags may be overwritten.

The combination `TYPE_A_700=1` and `HW_ELTPOW=1` with random input does not work. The 'random' input is all zeros. A workaround is available in the file `A_700-inputs.txt` that can be used by replacing the static input in `pairing.c`. This bug does not seem to affect correctness of the output.
