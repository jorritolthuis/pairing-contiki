###
### Pairing evaluation on CC2538 (RE-Mote Rev B)
###

### Test configurations
# Generating random inputs is not part of the comparison
# Run i to measure timing is different from run i to measure power
# All deterministic runs have the same input
#                  | c1 | c2 | c3 | c4 | c5 | c6 | c7 | c8 |
# -----------------|----|----|----|----|----|----|----|----|
# DETERMINISTIC    |    |    |    |    |  X |  X |  X |  X |
# HW_ELTPOW        |    |  X |    |  X |    |  X |    |  X |
# HW_MODINV        |    |    |  X |  X |    |    |  X |  X |
#                  |    |    |    |    |    |    |    |    |
#

library(readr)
library(ggplot2)
library(gridExtra)

### Data

# c1
power_core_c1 <- list(read_csv("power/c1-r1-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c1-r2-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c1-r3-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c1-r4-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c1-r5-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c1-r6-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c1-r7-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c1-r8-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c1-r9-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c1-r10-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())))
power_full_c1 <- list(read_csv("power/c1-r1-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c1-r2-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c1-r3-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c1-r4-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c1-r5-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c1-r6-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c1-r7-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c1-r8-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c1-r9-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c1-r10-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())))

# c2
power_core_c2 <- list(read_csv("power/c2-r1-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c2-r2-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c2-r3-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c2-r4-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c2-r5-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c2-r6-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c2-r7-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c2-r8-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c2-r9-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c2-r10-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())))
power_full_c2 <- list(read_csv("power/c2-r1-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c2-r2-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c2-r3-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c2-r4-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c2-r5-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c2-r6-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c2-r7-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c2-r8-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c2-r9-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c2-r10-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())))

# c3
power_core_c3 <- list(read_csv("power/c3-r1-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c3-r2-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c3-r3-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c3-r4-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c3-r5-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c3-r6-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c3-r7-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c3-r8-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c3-r9-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c3-r10-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())))
power_full_c3 <- list(read_csv("power/c3-r1-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c3-r2-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c3-r3-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c3-r4-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c3-r5-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c3-r6-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c3-r7-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c3-r8-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c3-r9-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c3-r10-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())))

# c4
power_core_c4 <- list(read_csv("power/c4-r1-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c4-r2-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c4-r3-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c4-r4-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c4-r5-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c4-r6-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c4-r7-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c4-r8-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c4-r9-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c4-r10-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())))
power_full_c4 <- list(read_csv("power/c4-r1-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c4-r2-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c4-r3-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c4-r4-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c4-r5-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c4-r6-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c4-r7-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c4-r8-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c4-r9-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c4-r10-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())))

# c5
power_core_c5 <- list(read_csv("power/c5-r1-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c5-r2-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c5-r3-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c5-r4-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c5-r5-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c5-r6-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c5-r7-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c5-r8-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c5-r9-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c5-r10-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())))
power_full_c5 <- list(read_csv("power/c5-r1-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c5-r2-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c5-r3-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c5-r4-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c5-r5-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c5-r6-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c5-r7-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c5-r8-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c5-r9-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c5-r10-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())))

# c6
power_core_c6 <- list(read_csv("power/c6-r1-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c6-r2-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c6-r3-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c6-r4-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c6-r5-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c6-r6-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c6-r7-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c6-r8-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c6-r9-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c6-r10-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())))
power_full_c6 <- list(read_csv("power/c6-r1-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c6-r2-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c6-r3-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c6-r4-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c6-r5-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c6-r6-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c6-r7-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c6-r8-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c6-r9-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c6-r10-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())))

# c7
power_core_c7 <- list(read_csv("power/c7-r1-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c7-r2-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c7-r3-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c7-r4-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c7-r5-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c7-r6-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c7-r7-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c7-r8-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c7-r9-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c7-r10-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())))
power_full_c7 <- list(read_csv("power/c7-r1-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c7-r2-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c7-r3-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c7-r4-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c7-r5-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c7-r6-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c7-r7-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c7-r8-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c7-r9-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c7-r10-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())))

# c8
power_core_c8 <- list(read_csv("power/c8-r1-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c8-r2-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c8-r3-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c8-r4-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c8-r5-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c8-r6-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c8-r7-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c8-r8-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c8-r9-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c8-r10-trim-core.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())))
power_full_c8 <- list(read_csv("power/c8-r1-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c8-r2-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c8-r3-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c8-r4-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c8-r5-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c8-r6-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c8-r7-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c8-r8-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c8-r9-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())), read_csv("power/c8-r10-trim-full.csv", col_types = cols(dt=col_integer(), current=col_double(), voltage=col_double(), time=col_integer(), power=col_double())))

power_core <- list(power_core_c1, power_core_c2, power_core_c3, power_core_c4, power_core_c5, power_core_c6, power_core_c7, power_core_c8)
power_full <- list(power_full_c1, power_full_c2, power_full_c3, power_full_c4, power_full_c5, power_full_c6, power_full_c7, power_full_c8)

### Post-processing
align_time <- function(recording) {
	# Aligns the recording to start at time = 0
	start_time <- recording$time[1]
	for(i in 1:length(recording$time)) {
		recording$time[i] <- recording$time[i] - start_time
	}
	return(recording)
}

for (i in 1:length(power_core)) {
	for (j in 1:length(power_core[[i]])) {
		power_core[[i]][[j]] <- align_time(power_core[[i]][[j]])
	}
}

for (i in 1:length(power_full)) {
	for (j in 1:length(power_full[[i]])) {
		power_full[[i]][[j]] <- align_time(power_full[[i]][[j]])
	}
}
# NOTE: At this point, only use power_full and power_core (the configuration specific)
# tests are not updated by the code above

### Evaluation

# Plot all full
p1 <- ggplot(NULL,  mapping=aes(time, power))
for (i in 1:4) {
	p1 <- p1 + geom_line(data=as.data.frame(power_full[[1]][[i]]), col=i, mapping=aes(time, power), group=paste("Run",i))
}
p1 <- p1 + labs(x="Time [µs]", y="Power Usage [mW]", title="Power usage of full software execution")
p1 <- p1 + theme_bw() + xlim(0, 1.25e7) #+ theme(legend.position="right")
p1 <- p1 + scale_colour_discrete(name="Execution",labels=c("Run 1", "Run 2", "Run 3", "Run 4"))

p2 <- ggplot(NULL,  mapping=aes(time, power))
for (i in 1:4) {
	p2 <- p2 + geom_line(data=as.data.frame(power_full[[2]][[i]]), col=i, mapping=aes(time, power), group=paste("Run",i))
}
p2 <- p2 + labs(x="Time [µs]", y="Power Usage [mW]", title="Power usage of element exponentiation HWA execution")
p2 <- p2 + theme_bw() + xlim(0, 1.25e7) #+ theme(legend.position="right")
p2 <- p2 + scale_colour_discrete(name="Execution",labels=c("Run 1", "Run 2", "Run 3", "Run 4"))

p3 <- ggplot(NULL,  mapping=aes(time, power))
for (i in 1:4) {
	p3 <- p3 + geom_line(data=as.data.frame(power_full[[3]][[i]]), col=i, mapping=aes(time, power), group=paste("Run",i))
}
p3 <- p3 + labs(x="Time [µs]", y="Power Usage [mW]", title="Power usage of modular inversion HWA execution")
p3 <- p3 + theme_bw() + xlim(0, 1.25e7) #+ theme(legend.position="right")
p3 <- p3 + scale_colour_discrete(name="Execution",labels=c("Run 1", "Run 2", "Run 3", "Run 4"))

p4 <- ggplot(NULL,  mapping=aes(time, power))
for (i in 1:4) {
	p4 <- p4 + geom_line(data=as.data.frame(power_full[[4]][[i]]), col=i, mapping=aes(time, power), group=paste("Run",i))
}
p4 <- p4 + labs(x="Time [µs]", y="Power Usage [mW]", title="Power usage of full HWA execution")
p4 <- p4 + theme_bw() + xlim(0, 1.25e7) #+ theme(legend.position="right")
p4 <- p4 + scale_colour_discrete(name="Execution",labels=c("Run 1", "Run 2", "Run 3", "Run 4"))

grid.arrange(p1, p2, p3, p4, ncol=1,nrow=4)

# Plot all core
p1 <- ggplot(NULL,  mapping=aes(time, power))
for (i in 1:4) {
	p1 <- p1 + geom_line(data=as.data.frame(power_core[[1]][[i]]), col=i, mapping=aes(time, power), group=paste("Run",i))
}
p1 <- p1 + labs(x="Time [us]", y="Power Usage [mW]", title="Power usage of full software execution")
p1 <- p1 + theme_bw() #+ theme(legend.position="right")
p1 <- p1 + scale_colour_discrete(name="Execution",labels=c("Run 1", "Run 2", "Run 3", "Run 4"))

p2 <- ggplot(NULL,  mapping=aes(time, power))
for (i in 1:4) {
	p2 <- p2 + geom_line(data=as.data.frame(power_core[[2]][[i]]), col=i, mapping=aes(time, power), group=paste("Run",i))
}
p2 <- p2 + labs(x="Time [us]", y="Power Usage [mW]", title="Power usage of element exponentiation HWA execution")
p2 <- p2 + theme_bw() #+ theme(legend.position="right")
p2 <- p2 + scale_colour_discrete(name="Execution",labels=c("Run 1", "Run 2", "Run 3", "Run 4"))

p3 <- ggplot(NULL,  mapping=aes(time, power))
for (i in 1:4) {
	p3 <- p3 + geom_line(data=as.data.frame(power_core[[3]][[i]]), col=i, mapping=aes(time, power), group=paste("Run",i))
}
p3 <- p3 + labs(x="Time [us]", y="Power Usage [mW]", title="Power usage of modular inversion HWA execution")
p3 <- p3 + theme_bw() #+ theme(legend.position="right")
p3 <- p3 + scale_colour_discrete(name="Execution",labels=c("Run 1", "Run 2", "Run 3", "Run 4"))

p4 <- ggplot(NULL,  mapping=aes(time, power))
for (i in 1:4) {
	p4 <- p4 + geom_line(data=as.data.frame(power_core[[4]][[i]]), col=i, mapping=aes(time, power), group=paste("Run",i))
}
p4 <- p4 + labs(x="Time [us]", y="Power Usage [mW]", title="Power usage of full HWA execution")
p4 <- p4 + theme_bw() #+ theme(legend.position="right")
p4 <- p4 + scale_colour_discrete(name="Execution",labels=c("Run 1", "Run 2", "Run 3", "Run 4"))

grid.arrange(p1, p2, p3, p4, ncol=1,nrow=4)

### Do actual computations on the energy usage
calculate_power <- function(core) {
	power_total <- 0.0
	for (i in 1:length(core$dt)) {
		power_total <- power_total + (core$dt[i] * core$power[i])
	}
	return(power_total)
}

power_c1 = c()
for (i in 1:length(power_core_c1)) { power_c1 <- c(power_c1, calculate_power(power_core_c1[[i]])) }
print(paste("Mean power c1:", mean(power_c1)/1e9, "[J] SD c1:", sd(power_c1)/1e9))
power_c2 = c()
for (i in 1:length(power_core_c2)) { power_c2 <- c(power_c2, calculate_power(power_core_c2[[i]])) }
print(paste("Mean power c2:", mean(power_c2)/1e9, "[J] SD c2:", sd(power_c2)/1e9))
power_c3 = c()
for (i in 1:length(power_core_c3)) { power_c3 <- c(power_c3, calculate_power(power_core_c3[[i]])) }
print(paste("Mean power c3:", mean(power_c3)/1e9, "[J] SD c3:", sd(power_c3)/1e9))
power_c4 = c()
for (i in 1:length(power_core_c4)) { power_c4 <- c(power_c4, calculate_power(power_core_c4[[i]])) }
print(paste("Mean power c4:", mean(power_c4)/1e9, "[J] SD c4:", sd(power_c4)/1e9))
power_c5 = c()
for (i in 1:length(power_core_c5)) { power_c5 <- c(power_c5, calculate_power(power_core_c5[[i]])) }
print(paste("Mean power c5:", mean(power_c5)/1e9, "[J] SD c5:", sd(power_c5)/1e9))
power_c6 = c()
for (i in 1:length(power_core_c6)) { power_c6 <- c(power_c6, calculate_power(power_core_c6[[i]])) }
print(paste("Mean power c6:", mean(power_c6)/1e9, "[J] SD c6:", sd(power_c6)/1e9))
power_c7 = c()
for (i in 1:length(power_core_c7)) { power_c7 <- c(power_c7, calculate_power(power_core_c7[[i]])) }
print(paste("Mean power c7:", mean(power_c7)/1e9, "[J] SD c7:", sd(power_c7)/1e9))
power_c8 = c()
for (i in 1:length(power_core_c8)) { power_c8 <- c(power_c8, calculate_power(power_core_c8[[i]])) }
print(paste("Mean power c8:", mean(power_c8)/1e9, "[J] SD c8:", sd(power_c8)/1e9))

print(paste("c2 is ", -100*(mean(power_c2)-mean(power_c1))/mean(power_c1), "% more energy efficient than c1"))
print(paste("c3 is ", -100*(mean(power_c3)-mean(power_c1))/mean(power_c1), "% more energy efficient than c1"))
print(paste("c4 is ", -100*(mean(power_c4)-mean(power_c1))/mean(power_c1), "% more energy efficient than c1"))

# Relate power usage to timing
time  <- 			c(2945404, 	2942092, 	2965151, 	2960363, 	2941336, 			2944467, 			2967230, 			2963027)
energy <- 			c(.375903, 	.376609, 	.379646, 	.379794, 	.375032, 			.372708, 			.374215, 			.371044)
deterministic <- 	c("Random", "Random", 	"Random", 	"Random", 	"Deterministic", 	"Deterministic", 	"Deterministic", 	"Deterministic")
df <- data.frame(timing=time, energy_usage=energy, group=deterministic)

correlation_plot <- ggplot(df, aes(x=timing, y=energy_usage, color=group)) 
correlation_plot <- correlation_plot + geom_point() + geom_smooth(method=lm)
correlation_plot

# Now do the same for all separate measurements
time <- c()
energy <- c()
cat <- c()

energies <- list(power_c1, power_c2, power_c3, power_c4, power_c5, power_c6, power_c7, power_c8)
categories <- c("C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8")

for (i in 1:length(energies)) {
	for (j in 1:length(energies[[i]])) {
		# Not as accurate as rtimer time, but I don't have time measurements for the energy measurement runs
		time <- c(time, power_core[[i]][[j]]$time[length(power_core[[i]][[j]]$time)])
		energy <- c(energy, energies[[i]][j])
		cat <- c(cat, categories[i])
	}
}

df <- data.frame(time=time, energy=energy, group=cat)
p_compare_runs <- ggplot(df, aes(x=time, y=energy, color=group)) + geom_point()
grid.arrange(p_compare_runs, ncol=1,nrow=1)



