###
### Pairing evaluation on CC2538 (RE-Mote Rev B)
###

### Test configurations (labels were assigned in no particular order)
#     | Curve type | Security level | HW acceleration |
# C1  |     A      |       636      |       None      |
# C2  |     A      |      1024      |       None      |
# C3  |     F      |       636      |       None      |
# C4  |     F      |       636      |      HW_ECC     |
# C5  |     A      |      1024      |      HW_MUL     |
# C6  |     A      |       636      |      HW_MUL     |
# C7  |     A      |      1024      |     HW_MODINV   |
# C8  |     A      |      1024      |     HW_ELTPOW   |
# C9  |     A      |       636      |      HW_ECC     | Note: C9 with 1024 crashes
# C10 |     A      |       636      |     HW_ELTPOW   |
# C11 |     A      |      1400      |       None      |
# C12 |     A      |      1400      | ELTPOW + MODINV |
# C13 |     F      |       636      |      HW_MUL     | <- build script should ensure this is equal to c3
# C14 |     A      |       636      |     HW_MODINV   |
# C15 |     F      |       636      |     HW_MODINV   |
# C16 |     F      |       636      |     HW_ELTPOW   |
# C17 |     A      |       636      | ELTPOW + MODINV |
# C18 |     A      |      1024      | ELTPOW + MODINV |
# C19 |     A      |      1400      |     HW_ELTPOW   | NEXT DAY!
# C20 |     A      |      1400      |     HW_MODINV   |
# C21 |     A      |      1400      |     HW_MODINV   | NEXT DAY (same as C20)!
# C22 |     A      |      1400      |       None      | NEXT DAY (same as C11)!
# C23 |     A      |      1400      | ELTPOW + MODINV | NEXT DAY (same as C12)!

### Data
ticks_per_second = 32768

# Source data, in order of measurement
c2_ticks	= c(	64928, 	64907, 	64894, 	64878, 	64938, 	64877, 	64908, 	64885, 	64885, 	64881, 
					64874, 	64913, 	64902, 	64895, 	64884, 	64914, 	64919, 	64911, 	64872, 	64911)
c1_ticks	= c(	45135, 	45152, 	45119, 	45126, 	45151, 	45149, 	45108, 	45130, 	45128, 	45136, 
					45146, 	45135, 	45120, 	45099, 	45157, 	45130, 	45129, 	45137, 	45147, 	45120)
c11_ticks	= c(	233368, 233306, 233129, 233299, 233253, 233234, 233381, 233180, 233135, 233380, 
					233381, 233246, 233333, 233284, 233131, 233154, 233253, 233420, 233411, 233168)
c3_ticks	= c(	83627,	83625,	83675,	83669,	83630,	83624,	83583,	83680,	83632,	83653,
					83591,	83631,	83666,	83645,	83636,	83647,	83638,	83669,	83674,	83685)
c4_ticks	= c(	26966,	26922,	26874,	26896,	26919,	26893, 	26924,	26908,	26904,	26927,
					26863,	26906,	26889,	26870,	26898,	26885,	26910,	26886,	26925,	26892)
c9_ticks	= c(	45127,	45144, 	45121,	45095,	45117,	45129,	45145,	45108,	45102,  45144,
					45135,	45145,	45108,	45135,	45111,	45138,	45135,	45108,	45101,	45129)
c13_ticks	= c(	83615,	83629,	83628,	83671,	83664,	83650,	83635,	83630,	83673,	83653,
					83612,	83629,	83632,	83624,	83647,	83635,	83602,	83600,	83647,	83648)
c5_ticks	= c(	114997, 114955,	114999,	114962,	114992, 114979, 114986,	114980, 115001, 114995,
					114981, 115021,	115035,	115001,	115037,	114987,	115007,	114972,	115016,	115023)
c6_ticks	= c(	91154,	91154,	91158,	91174,	91205,	91179,	91169,	91188,	91195,	91187,
					91160,	91188,	91189,	91169,	91152,	91183,	91177,	91216,	91181,	91169)
c14_ticks	= c(	43480,	43490,	43469,	43435,	43472,	43460,	43512,	43481,	43467,	43470,
					43483,	43496,	43447,	43477,	43456,	43475,	43504,	43507,	43445,	43476)
c7_ticks	= c(	62265,	62282,	62234,	62248,	62252,	62223,	62241,	62246,	62217,	62268,
					62270,	62295,	62216,	62197,	62223,	62225,	62260,	62249,	62265,	62221)
c15_ticks	= c(	85482,	85429, 	85458, 	85466, 	85444, 	85421, 	85431, 	85454, 	85482, 	85457,
				 	85487, 	85499, 	85449, 	85435, 	85421, 	85403, 	85446, 	85464, 	85442, 	85468)
c10_ticks	= c(	43223, 	43251,	43229,	43213,	43205,	43223,	43252,	43264,	43242,	43243,
					43229,	43226,	43247,	43234,	43240,	43234,	43217,	43241,	43261,	43253)
c8_ticks	= c(	61942,	61921, 	61914, 	61935, 	61961, 	61979, 	61937, 	61933, 	61942, 	61917,
				 	61932, 	61997, 	61905, 	61945, 	61949, 	61922, 	61918, 	61956, 	61949, 	61939)
c16_ticks	= c(	83612,	83593,	83646,	83657,	83603,	83600,	83645,	83619,	83655,	83667,
					83664,	83641,	83656,	83602,	83616,	83634,	83646,	83619,	83615,	83600)
c17_ticks	= c(	43412,	43429,	43419,	43414,	43415,	43406,	43386,	43453,	43412,	43368,
					43386,	43394,	43404,	43404,	43431,	43404,	43430,	43408,	43403,	43394)
c18_ticks	= c(	62146,	62146,	62169,	62197,	62184,	62183,	62149,	62171,	62160,	62163,
					62157,	62157,	62200,	62159,	62160,	62179,	62104,	62151,	62204,	62137)
c12_ticks	= c(	221337,	221344,	221255,	221156,	221154,	221294,	221364,	221405,	221185,	221166,
					221167,	221195,	221185,	221312,	221174,	221336,	221318,	221135,	221229,	221168)
c20_ticks	= c(	221392,	221449,	221417,	221465,	221390,	221397,	221388,	221386,	221554,	221485,
					221559,	221362,	221530,	221431,	221411,	221371,	221419,	221541,	221505,	221608)
# Tested next day:
c19_ticks	= c(	232900,	233062,	233104,	233973,	232973,	232964,	232965,	232876,	232875, 232876,
					232932,	233113,	232936,	233030,	233130,	233078,	233097,	233056,	233010,	233023)
c21_ticks	= c(	233810,	233991,	233986,	234058,	233792,	233805,	233942,	233778,	233958,	233920,
					233853,	233798,	233993,	233817,	233820,	233861,	233913,	234016,	233794,	234020)
c22_ticks	= c(	220439,	220443,	220352,	220377,	220397,	220290,	220255,	220427,	220342,	220247,
					220441,	220252,	220265,	220325,	220322,	220414,	220468,	220358,	220346,	220197)
c23_ticks	= c(	234255,	234101,	234193,	234040,	234017,	234110,	234207,	234057,	234188,	234070,
					234233,	234292,	234105,	234158,	234020,	234052,	234214,	234146,	233978,	234026)

### Post-processing
c1_us = c()
for (t in c1_ticks) c1_us = c(c1_us, 1000000*t/ticks_per_second)
c2_us = c()
for (t in c2_ticks) c2_us = c(c2_us, 1000000*t/ticks_per_second)
c3_us = c()
for (t in c3_ticks) c3_us = c(c3_us, 1000000*t/ticks_per_second)
c4_us = c()
for (t in c4_ticks) c4_us = c(c4_us, 1000000*t/ticks_per_second)
c5_us = c()
for (t in c5_ticks) c5_us = c(c5_us, 1000000*t/ticks_per_second)
c6_us = c()
for (t in c6_ticks) c6_us = c(c6_us, 1000000*t/ticks_per_second)
c7_us = c()
for (t in c7_ticks) c7_us = c(c7_us, 1000000*t/ticks_per_second)
c8_us = c()
for (t in c8_ticks) c8_us = c(c8_us, 1000000*t/ticks_per_second)
c9_us = c()
for (t in c9_ticks) c9_us = c(c9_us, 1000000*t/ticks_per_second)
c10_us = c()
for (t in c10_ticks) c10_us = c(c10_us, 1000000*t/ticks_per_second)
c11_us = c()
for (t in c11_ticks) c11_us = c(c11_us, 1000000*t/ticks_per_second)
c12_us = c()
for (t in c12_ticks) c12_us = c(c12_us, 1000000*t/ticks_per_second)
c13_us = c()
for (t in c13_ticks) c13_us = c(c13_us, 1000000*t/ticks_per_second)
c14_us = c()
for (t in c14_ticks) c14_us = c(c14_us, 1000000*t/ticks_per_second)
c15_us = c()
for (t in c15_ticks) c15_us = c(c15_us, 1000000*t/ticks_per_second)
c16_us = c()
for (t in c16_ticks) c16_us = c(c16_us, 1000000*t/ticks_per_second)
c17_us = c()
for (t in c17_ticks) c17_us = c(c17_us, 1000000*t/ticks_per_second)
c18_us = c()
for (t in c18_ticks) c18_us = c(c18_us, 1000000*t/ticks_per_second)
c19_us = c()
for (t in c19_ticks) c19_us = c(c19_us, 1000000*t/ticks_per_second)
c20_us = c()
for (t in c20_ticks) c20_us = c(c20_us, 1000000*t/ticks_per_second)
c21_us = c()
for (t in c21_ticks) c21_us = c(c21_us, 1000000*t/ticks_per_second)
c22_us = c()
for (t in c22_ticks) c22_us = c(c22_us, 1000000*t/ticks_per_second)
c23_us = c()
for (t in c23_ticks) c23_us = c(c23_us, 1000000*t/ticks_per_second)

### Evaluation

# Simple boxplots for each of the configurations
dev.new()
boxplot(c1_us, c2_us, c3_us, c4_us, xlab="Configuration", ylab="Execution time [us]", names=c("c1", "c2", "c3", "c4"))

# Print (and save means and standard deviations)
print(paste("Mean c1 [us]: ", mean(c1_us), "  SD c1:", sd(c1_us)))
print(paste("Mean c2 [us]: ", mean(c2_us), "  SD c2:", sd(c2_us)))
print(paste("Mean c3 [us]: ", mean(c3_us), "  SD c3:", sd(c3_us)))
print(paste("Mean c4 [us]: ", mean(c4_us), "  SD c4:", sd(c4_us)))
print(paste("Mean c5 [us]: ", mean(c5_us), "  SD c5:", sd(c5_us)))
print(paste("Mean c6 [us]: ", mean(c6_us), "  SD c6:", sd(c6_us)))
print(paste("Mean c7 [us]: ", mean(c7_us), "  SD c7:", sd(c7_us)))
print(paste("Mean c8 [us]: ", mean(c8_us), "  SD c8:", sd(c8_us)))
print(paste("Mean c9 [us]: ", mean(c9_us), "  SD c9:", sd(c9_us)))
print(paste("Mean c10 [us]: ", mean(c10_us), "  SD c10:", sd(c10_us)))
print(paste("Mean c11 [us]: ", mean(c11_us), "  SD c11:", sd(c11_us)))
print(paste("Mean c12 [us]: ", mean(c12_us), "  SD c12:", sd(c12_us)))
print(paste("Mean c13 [us]: ", mean(c13_us), "  SD c13:", sd(c13_us)))
print(paste("Mean c14 [us]: ", mean(c14_us), "  SD c14:", sd(c14_us)))
print(paste("Mean c15 [us]: ", mean(c15_us), "  SD c15:", sd(c15_us)))
print(paste("Mean c16 [us]: ", mean(c16_us), "  SD c16:", sd(c16_us)))
print(paste("Mean c17 [us]: ", mean(c17_us), "  SD c17:", sd(c17_us)))
print(paste("Mean c18 [us]: ", mean(c18_us), "  SD c18:", sd(c18_us)))
print(paste("Mean c19 [us]: ", mean(c19_us), "  SD c19:", sd(c19_us)))
print(paste("Mean c20 [us]: ", mean(c20_us), "  SD c20:", sd(c20_us)))
print(paste("Mean c21 [us]: ", mean(c21_us), "  SD c21:", sd(c21_us)))
print(paste("Mean c22 [us]: ", mean(c22_us), "  SD c22:", sd(c22_us)))
print(paste("Mean c23 [us]: ", mean(c23_us), "  SD c23:", sd(c23_us)))

