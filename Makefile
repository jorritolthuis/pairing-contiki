CONTIKI_PROJECT = pairing

CFLAGS:=  -I. \
	  -Igmp 

CFLAGS+= $(FLAGS) 

ifeq ($(TARGET),native)
	CFLAGS+= -ggdb
endif

ifneq ($(TARGET),native)
	CFLAGS+= -specs=nosys.specs
	LDFLAGS+= -specs=nosys.specs
endif

TARGET_LIBFILES:= -Lpbc -lpbc \
	  -Lgmp -lgmp

# This should be set automatically in Makefile.include, but somehow
# I ended up with an empty variable. This fixes it
ifeq ($(TARGET),native)
	CONTIKI_NG_PROJECT_MAP:=build/native/native/$(CONTIKI_PROJECT).map
else ifeq ($(TARGET),openmote)
	CONTIKI_NG_PROJECT_MAP:=build/openmote/$(BOARD)/$(CONTIKI_PROJECT).map
else ifeq ($(TARGET),zoul)
	CONTIKI_NG_PROJECT_MAP:=build/zoul/$(BOARD)/$(CONTIKI_PROJECT).map
else
	# Do not set anything custom
endif

all: $(CONTIKI_PROJECT)

CONTIKI = ../..
include $(CONTIKI)/Makefile.include
