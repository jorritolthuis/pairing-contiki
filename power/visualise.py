import pandas as pd
import plotly.express as px
import sys
import tqdm

print(sys.argv[1])
df = pd.read_csv(sys.argv[1])

df['time'] = 0
df['power'] = 0

print(df.head())
print(df.columns)

for index, row in tqdm.tqdm(df.iterrows()):
	#row['time'] = df.iloc[:index].sum()
	#for i in range(index):
	#	row['time'] = row['time'] + df.iloc[i]['dt']
	if index > 0:
		row['time'] = df.iloc[index-1]['time'] + row['dt']
	print(row['time'], row['dt'])
	#print("Index", index, "has time", row['time'])
	#print("Current value is", row[' current'], "voltage is", row[' voltage'])
	row['power'] = row[' current'] * row[' voltage']

print(df.head())
fig = px.line(df, x = 'time', y = 'power', title='Power usage of pairing on RE-Mote')
fig.show()
