# Align and trim the power recordings
# Takes unaligned recording as first argument and creates two outputs:
# - Full exection (from release of reset button to end of program)
# - Pairing (period between the two 2-second sleep periods)
# Note: this script HEAVILY depends on the specific characteristics of
# this particular execution.

import pandas as pd
#import plotly.express as px
import sys
import tqdm

print(sys.argv[1])
df = pd.read_csv(sys.argv[1])

df['time'] = 0
df['power'] = 0

dt = df['dt'].to_list()
current = df[' current'].to_list()
voltage = df[' voltage'].to_list()

time = [0]
power = []

# Calculate time and power columns
for i in tqdm.tqdm(range(len(dt))):
	power.append(current[i]*voltage[i])
	if i > 0:
		time.append(time[i-1] + dt[i])
	

# Register important timestamps
reset_up = -1 # Point in time where reset button is released
power_up = -1 # Point where the board seems to start up
sleep_start = -1 # Point where the board start 
pairing_start = -1 # Point where the 2 seconds sleep ends, and pairing starts
pairing_end = -1 # Point where the pairing has finished, and the second 2-second sleep starts
sleep_end = -1 # Point where the second 2-second sleep ends
final_end = -1 # After finishing the final wakeup, control is handed back to Contiki-NG

# states
st_reset_down = 1 # Waiting for reset to go down
st_reset_up = 2 # Waiting for reset to go back up
st_power_up = 3 # Waiting for power to go up significantly
st_peak = 10 # Wait for the highest peak
st_sleep_1 = 4 # Waiting to go into first sleep
st_pairing = 5 # Waiting to start pairing
st_sleep_2 = 6 # Waiting for pairing to finish
st_sleep_2_end = 7 # Waiting for second sleep to end
st_finish = 8 # Waiting until the control gets handed back to Contiki
st_end = 9

cur_state = st_reset_down

for i in tqdm.tqdm(range(len(power))):
	if cur_state == st_reset_down:
		if power[i] < 20:
			cur_state = st_reset_up
	elif cur_state == st_reset_up:
		if power[i] > 20:
			reset_up = i
			cur_state = st_power_up
	elif cur_state == st_power_up:
		if power[i] > 110:
			power_up = i
			cur_state = st_peak
	elif cur_state == st_peak:
		if power[i] > 165:
			cur_state = st_sleep_1
	elif cur_state == st_sleep_1:
		if power[i] < 126:
			sleep_start = i
			cur_state = st_pairing
	elif cur_state == st_pairing:
		if power[i] > 150:
			pairing_start = i
			cur_state = st_sleep_2
	elif cur_state == st_sleep_2:
		if power[i] < 126:
			pairing_end = i-1
			cur_state = st_sleep_2_end
	elif cur_state == st_sleep_2_end:
		if power[i] > 135:
			sleep_end = i
			cur_state = st_finish
	elif cur_state == st_finish:
		if power[i] < 115:
			final_end = i-1
			cur_state = st_end
	elif cur_state == st_end:
		break
	else:
		print("ERROR: Undefined state", cur_state)
		exit(-1)
		
print("Reset up", reset_up, time[reset_up])
print("Power up", power_up, time[power_up])
print("Sleep start", sleep_start, time[sleep_start])
print("Pairing start", pairing_start, time[pairing_start])
print("Pairing end", pairing_end, time[pairing_end])
print("Sleep end", sleep_end, time[sleep_end])
print("Final end", final_end, time[final_end])
print("Length", len(dt), time[len(dt)-1])

full_trim = list(zip(dt[reset_up:final_end], current[reset_up:final_end], voltage[reset_up:final_end], time[reset_up:final_end], power[reset_up:final_end]))
pairing_trim = list(zip(dt[pairing_start:pairing_end], current[pairing_start:pairing_end], voltage[pairing_start:pairing_end], time[pairing_start:pairing_end], power[pairing_start:pairing_end]))

df_full = pd.DataFrame(full_trim, columns=['dt', 'current', 'voltage', 'time', 'power'])
df_pairing = pd.DataFrame(pairing_trim, columns=['dt', 'current', 'voltage', 'time', 'power'])

full_filename = sys.argv[1].replace(".csv", "-aligned.csv")
pairing_filename = sys.argv[1].replace(".csv", "-trimmed.csv")

df_full.to_csv(full_filename, index=False)
print("Written to file", full_filename, "with ", final_end-reset_up, "indices")
df_pairing.to_csv(pairing_filename, index=False)
print("Written to file", pairing_filename, "with ", pairing_end-pairing_start, "indices")

