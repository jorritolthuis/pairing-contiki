# Power measurements
Configuration 2 and 4 were identified during the timing analysis as the most interesting to perform power analysis for. This folder contains the measurement data for these configurations.

Not all measurements in this directory are unique. Some measurements were taken from other locations where I had already stored results for earlier tests (e.g. the consistency tests for rtimer with configuration 2).

c4-11 through c4-14 were used to identify if the SLEEP_DURING_HW flag was enabled for the tests. Conclusion: It probably was enabled.

The data from c4 is also used to show the consistency in timing obtained from power measurements.
