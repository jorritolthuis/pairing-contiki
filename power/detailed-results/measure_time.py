# Uses all files ending in -trimmed.csv in the current directory

import os

files = os.listdir('.')
relevant_files = []

for file in files:
	if file.endswith("-trimmed.csv"):
		relevant_files += [file]

relevant_files.sort()
print(relevant_files)
		
for file in relevant_files:
	f = open(file, "rt")
	lines = f.readlines()
	first_line = lines[1].split(",")
	last_line = lines[-1].split(",")
	
	diff = int(last_line[3]) - int(first_line[3])
	
	print(file, "\t", diff/1e6)
