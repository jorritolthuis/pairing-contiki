# Align and trim the power recordings
# Takes unaligned recording as first argument and creates two outputs:
# - Full exection (from release of reset button to end of program)
# - Pairing (period between the two 2-second sleep periods)
# Note: this script HEAVILY depends on the specific characteristics of
# this particular execution.

import pandas as pd
#import plotly.express as px
import sys
import tqdm
import os

# Filter files
files = os.listdir('.')
relevant_files = []

for file in files:
	if file.endswith("-trimmed.csv"):
		relevant_files += [file]

relevant_files.sort()

# For all files
for file in relevant_files:
	df = pd.read_csv(file)

	dt = df['dt'].to_list()
	current = df['current'].to_list()
	voltage = df['voltage'].to_list()
	time = df['time'].to_list()
	power = df['power']

	total_energy = 0.0

	# Calculate time and power columns
	for i in range(len(dt)):
		total_energy += (dt[i]/1e6) * (power[i]/1e3) # Convert to Watt seconds
	
	print(file, "\t", total_energy)
