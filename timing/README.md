# Time measurements
Because the rtimer measurements have turned out not to be reliable, power measurements were used to extract timing as well.

Before and after the pairing operation, there is a sleep period. This allows one to identify the execution time of the operation (there are script to perform the analysis, albeit not super reliably).

Each configuration has 3 measurements (not all measurements were used, there are some errors). The file timing-results.r describes the differences configurations.
