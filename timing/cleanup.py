# Align and trim the power recordings
# Takes unaligned recording as first argument and creates two outputs:
# - Full exection (from release of reset button to end of program)
# - Pairing (period between the two 2-second sleep periods)
# Note: this script HEAVILY depends on the specific characteristics of
# this particular execution.

import pandas as pd
#import plotly.express as px
import sys
import tqdm

print(sys.argv[1])
df = pd.read_csv(sys.argv[1])

df['time'] = 0
df['power'] = 0

dt = df['dt'].to_list()
current = df[' current'].to_list()
voltage = df[' voltage'].to_list()

time = [0]
power = []

# Calculate time and power columns
for i in tqdm.tqdm(range(len(dt))):
	power.append(current[i]*voltage[i])
	if i > 0:
		time.append(time[i-1] + dt[i])

full = list(zip(dt, current, voltage, time, power))

df_full = pd.DataFrame(full, columns=['dt', 'current', 'voltage', 'time', 'power'])
df_pairing = pd.DataFrame(full, columns=['dt', 'current', 'voltage', 'time', 'power'])

full_filename = sys.argv[1].replace(".csv", "-aligned.csv")
pairing_filename = sys.argv[1].replace(".csv", "-trimmed.csv")

df_full.to_csv(full_filename, index=False)
print("Written to file", full_filename)
df_pairing.to_csv(pairing_filename, index=False)
print("Written to file", pairing_filename)

