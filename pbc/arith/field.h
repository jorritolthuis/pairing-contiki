#pragma once

#include <gmp.h>
#include "pbc_field.h"

void generic_mul_mpz(element_ptr r, element_ptr a, mpz_ptr z);
