#!/bin/bash

# Input validation
if [[ -z $TARGET ]]; then
	echo "ERROR: No TARGET specified"
	exit 1
elif [[ $TARGET == "native" ]]; then
	# Nothing to do
	echo -n ""
elif [[ $TARGET == "openmote" ]]; then
	# Check BOARD variable
	if [[ -z $BOARD ]]; then
		echo "$TARGET requires BOARD to be specified"
		exit 1
	elif [[ $BOARD == "openmote-cc2538" || $BOARD == "openmote-b" ]]; then
		# Nothing to do
		echo -n ""
	else
		echo "Unknown BOARD=$BOARD, use \"openmote-cc2538\", or \"openmote-b\""
		exit 1
	fi
elif [[ $TARGET == "zoul" ]]; then
	# Check BOARD variable
	if [[ -z $BOARD ]]; then
		echo "$TARGET requires BOARD to be specified"
		exit 1
	elif [[ $BOARD == "firefly" || $BOARD == "firefly-reva" || $BOARD == "orion" || $BOARD == "remote-reva" || $BOARD == "remote-revb" ]]; then
		# Nothing to do
		echo -n ""
	else
		echo "Unknown BOARD=$BOARD, use \"firefly\", \"firefly-reva\", \"orion\", \"remote-reva\", or \"remote-revb\""
		exit 1
	fi
else
	echo "Unrecognised TARGET=$TARGET, use \"native\", \"openmote\", or \"zoul\"."
	exit 1
fi

if [[ $TARGET != "native" ]]; then
	if [[ $CONTIKI_RANDOM != 1 ]]; then
		echo "CONTIKI_RANDOM is required for $TARGET, this option is enabled automatically."
		CONTIKI_RANDOM=1
	fi
fi

# Input validation passed, print configuration
echo -en "\e[31m"
echo -n "Building TARGET=$TARGET "
if [[ -n $BOARD ]]; then
	echo -n "BOARD=$BOARD, with settings "
else
	echo -n ", with settings: "
fi
if [[ $CONTIKI_RANDOM == 1 ]]; then 
	echo -n "CONTIKI_RANDOM "
fi
if [[ $HW_MODINV == 1 ]]; then
	if [[ $TARGET == "native" ]]; then
		HW_MODINV=0
	else
		echo -n "HW_MODINV "
	fi
fi
if [[ $HW_ELTPOW == 1 ]]; then
	if [[ $TARGET == "native" ]]; then
		HW_ELTPOW=0
	else
		echo -n "HW_ELTPOW "
	fi
fi
if [[ $HW_ECC == 1 ]]; then
	if [[ $TARGET == "native" ]]; then
		HW_ECC=0
	else
		echo -n "HW_ECC "
	fi
fi
if [[ $HW_MUL == 1 ]]; then
	if [[ $TARGET == "native" ]]; then
		HW_MUL=0
	else
		echo -n "HW_MUL "
	fi
fi
if [[ $DETERMINISTIC == 1 ]]; then
	echo -n "DETERMINISTIC "
fi
if [[ $TIMING == 1 ]]; then
	if [[ $TARGET == "zoul" || $TARGET == "remote" ]]; then
		echo -n "TIMING "
	else
		TIMING=0
	fi
fi
if [[ $POWER == 1 ]]; then
	if [[ $TARGET == "zoul" || $TARGET == "remote" ]]; then
		echo -n "POWER "
	else
		POWER=0
	fi
fi
if [[ $KEEP_PKA_ON == 1 ]]; then
	echo -n " KEEP_PKA_ON "
fi
if [[ $SLEEP_DURING_HW == 1 ]]; then
	echo -n " SLEEP_DURING_HW "
fi
if [[ $TYPE_F == 1 ]]; then
	# HW_MUL has only been tested for TYPE_A
	HW_MUL=0
	
	TYPE_A_318=0
	TYPE_A_512=0
	TYPE_A_700=0
	echo -n " TYPE_F "
elif [[ $TYPE_A_318 == 1 ]]; then
	TYPE_A_512=0
	TYPE_A_700=0
	echo -n " TYPE_A_318 "
elif [[ $TYPE_A_700 == 1 ]]; then
	TYPE_A_512=0
	echo -n " TYPE_A_700 "
else
	TYPE_A_512=1
	echo -n " TYPE_A_512 "
fi
echo -e "\e[0m"

### START BUILD ###

if [[ $TARGET = native ]]; then
	# BUILD FOR PC
	FLAGS=""
	if [[ $CONTIKI_RANDOM == 1 ]]; then
		FLAGS+=" -DCONTIKI_RANDOM "
	fi
	if [[ $DETERMINISTIC == 1 ]]; then
		FLAGS+=" -DDETERMINISTIC "
	fi
	if [[ $TYPE_F == 1 ]]; then
		FLAGS+=" -DTYPE_F "
	fi
	if [[ $TYPE_A_318 == 1 ]]; then
		FLAGS+=" -DTYPE_A_318 "
	fi
	if [[ $TYPE_A_512 == 1 ]]; then
		FLAGS+=" -DTYPE_A_512 "
	fi
	if [[ $TYPE_A_700 == 1 ]]; then
		FLAGS+=" -DTYPE_A_700 "
	fi

	cd gmp
	make clean
	rm gen-bases gen-fac gen-fib gen-jacobitab gen-psqr gen-trialdivtab
	./configure CFLAGS="$FLAGS"
	make
	cd ../pbc
	make clean
	./configure CFLAGS="-I../../.. $FLAGS"
	make
	cd ..
	make TARGET=$TARGET clean
	make TARGET=$TARGET FLAGS="$FLAGS"
else
	# BUILD FOR ARM
	FLAGS=""
	if [[ $CONTIKI_RANDOM == 1 ]]; then
		FLAGS+=" -DCONTIKI_RANDOM "
	fi
	if [[ $HW_MODINV == 1 ]]; then
		FLAGS+=" -DHW_MODINV "
	fi
	if [[ $HW_ELTPOW == 1 ]]; then
		FLAGS+=" -DHW_ELTPOW "
	fi
	if [[ $HW_ECC == 1 ]]; then
		FLAGS+=" -DHW_ECC "
	fi
	if [[ $HW_MUL == 1 ]]; then
		FLAGS+=" -DHW_MUL "
	fi
	if [[ $DETERMINISTIC == 1 ]]; then
		FLAGS+=" -DDETERMINISTIC "
	fi
	if [[ $TIMING == 1 ]]; then
		FLAGS+=" -DTIMING "
	fi
	if [[ $POWER == 1 ]]; then
		FLAGS+=" -DPOWER "
	fi
	if [[ $KEEP_PKA_ON == 1 ]]; then
		FLAGS+=" -DKEEP_PKA_ON "
	fi
	if [[ $SLEEP_DURING_HW == 1 ]]; then
		FLAGS+=" -DSLEEP_DURING_HW "
	fi
	if [[ $TYPE_F == 1 ]]; then
		FLAGS+=" -DTYPE_F "
	fi
	if [[ $TYPE_A_318 == 1 ]]; then
		FLAGS+=" -DTYPE_A_318 "
	fi
	if [[ $TYPE_A_512 == 1 ]]; then
		FLAGS+=" -DTYPE_A_512 "
	fi
	if [[ $TYPE_A_700 == 1 ]]; then
		FLAGS+=" -DTYPE_A_700 "
	fi
	# Always use max clock speed on CC2538
	FLAGS+=" -DCC2538CPU "
	FLAGS+=" -DSYS_CTRL_CONF_SYS_DIV=0x00000000 -DSYS_CTRL_CONF_IO_DIV=0x00000000 "

	cd gmp
	CPU=cc2538
	make clean
	rm gen-bases gen-fac gen-fib gen-jacobitab gen-psqr gen-trialdivtab
	./configure --host=arm-none-eabi --build=x86_64-linux-gnu --disable-shared CFLAGS="$FLAGS -Os -pedantic -fomit-frame-pointer -ffunction-sections -fdata-sections -nostartfiles -specs=nosys.specs -mcpu=cortex-m3 -I$PWD/../../.. -I$PWD/../../../os -I$PWD/../../../arch/platform/$TARGET -I$PWD/../../../arch/cpu/$CPU -I$PWD/../../../arch/cpu/arm/cortex-m -I$PWD/../../../arch/cpu/arm -I$PWD/../../../arch/platform/$TARGET/$BOARD -I$PWD/../../../arch/cpu/arm/CMSIS/CMSIS/Core/Include" --disable-assembly
	make
	cd ../pbc
	make clean
	./configure --host=arm-none-eabi --build=x86_64-linux-gnu --disable-shared CFLAGS="$FLAGS -specs=nosys.specs -mcpu=cortex-m3 -ffunction-sections -fdata-sections -I$(readlink -f ../gmp) -I$(readlink -f ../../../) -I.. -I$PWD/../../.. -I$PWD/../../../os  -I$PWD/../../../arch/platform/$TARGET -I$PWD/../../../arch/cpu/cc2538 -I$PWD/../../../arch/cpu/cc2538/dev -I$PWD/../../../arch/cpu/arm/cortex-m -I$PWD/../../../arch/cpu/arm -I$PWD/../../../arch/platform/$TARGET/$BOARD -I$PWD/../../../arch/cpu/arm/CMSIS/CMSIS/Core/Include -I$PWD/../../../os/sys" LDFLAGS="-L$(readlink -f ../gmp)"
	make
	cd ..
	make TARGET=$TARGET BOARD=$BOARD clean
	make TARGET=$TARGET BOARD=$BOARD FLAGS="$FLAGS"
	if [[ $NO_STRIP != 1 ]]; then
		arm-none-eabi-strip --strip-unneeded pairing.$TARGET
	fi
	
	echo -e "\nFlash the executable on $BOARD using:\nsudo make TARGET=$TARGET BOARD=$BOARD FLAGS=\"$FLAGS\" pairing.upload" # Uploading recompiles part of the executable, so we need to set the flags properly.
fi


